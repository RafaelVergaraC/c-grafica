//

//  HelloCube

//

//  Created by Pedro  Cervantes Pintor on 11/Oct/14.

//
#define GLM_FORCE_RADIANS
#include "BaseApplication.h"
#include <iostream>
#include <vector>

#include "GL\glew.h"
#include <gl/GL.h>
#include <gl/GLU.h>
#include "GLFW\glfw3.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

#include "ShaderFuncs.h"



BaseApplication::BaseApplication() : vEye(0.0f, 50.0f, 100.0f),
							_drawMode (0),
							shader(0),
							_currentBuffer(0),
							_nextBuffer(1)
							
{
}

BaseApplication::~BaseApplication()
{
	glDeleteTextures(2, texturesID);
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(2, pboID);
}

void BaseApplication::initTextures()
{
	glGenTextures(2, texturesID);
	//Textura 0
	glBindTexture(GL_TEXTURE_2D, texturesID[0]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, WIDTH, HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, Buffers[0]);

	//Sin promediar
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);


	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	//Textura 1
	glBindTexture(GL_TEXTURE_2D, texturesID[1]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, WIDTH, HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, Buffers[1]);

	//Sin promediar
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);


	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	
	glBindTexture(GL_TEXTURE_2D, 0);
	

}

void BaseApplication::initPBOs()
{
	// create 2 pixel buffer objects, you need to delete them when program exits.
	// glBufferData with NULL pointer reserves only memory space.
	glGenBuffers(2, pboID);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pboID[0]);
	glBufferData(GL_PIXEL_UNPACK_BUFFER, WIDTH * HEIGHT * RGBA, 0, GL_STREAM_DRAW);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pboID[1]);
	glBufferData(GL_PIXEL_UNPACK_BUFFER, WIDTH * HEIGHT * RGBA, 0, GL_STREAM_DRAW);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
}


void BaseApplication::init()
{
	fakeBuffers();
	initTextures();
	initPBOs();
	GLfloat vertices[]
	{
		//triangulo 0


		-1.0f, -1.0f, 0.0f, 1.0f,

		1.0f, -1.0f, 0.0f, 1.0f,

		-1.0f, 1.0f, 0.0f, 1.0f,

		//triangulo2
		//-1.0f, 1.0f, 0.0f, 1.0f,

		//1.0f, -1.0f, 0.0f, 1.0f,

		1.0f, 1.0f, 0.0f, 1.0f,
	};
	GLfloat texCoords[] 
	{
		0.0f, 1.0f, 0.0f, 0.0f,  //TEX
		1.0f, 1.0f, 0.0f, 0.0f,//TEX
		0.0f, 0.0f, 0.0f, 0.0f, //TEX

		1.0f, 0.0f, 0.0f, 0.0f//TEX

		//0.0f, 1.0f, 0.0f, 0.0f,//TEX
		//1.0f, 0.0f, 0.0f, 0.0f,//TEX
	};

	vEye = glm::vec3(0.0f, 50.0f, 50.0f);

	std::string sVertex, sFragment;
	sVertex = loadTextFile("shaders/passThru.v");
	sFragment = loadTextFile("shaders/passThru.f");
	InitializeProgram(shaderID, sVertex, sFragment);
	
	uTransform = glGetUniformLocation(shaderID, "mTransform");
	sampler = glGetUniformLocation(shaderID, "theTexture");

	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices) + sizeof(texCoords), NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(vertices), sizeof(texCoords), texCoords);
	
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*)(sizeof(vertices)));

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	setup();
}


void BaseApplication::display()
{
	processPBO();

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glUseProgram(shaderID);

	mProjectionMatrix = glm::ortho(-1.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f);
	mTransform = glm::mat4(1.0f) * mProjectionMatrix;

	glUniformMatrix4fv(uTransform, 1, GL_FALSE, glm::value_ptr(mTransform));

	//pasamos parametros de textura
	glBindTexture(GL_TEXTURE_2D, texturesID[_currentBuffer]);
	glUniform1i(sampler, _currentBuffer);
	glActiveTexture(_currentBuffer==0? GL_TEXTURE0 : GL_TEXTURE1);
	//glActiveTexture(GL_TEXTURE0);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	swapbuffers();
}

void BaseApplication::processPBO()
{
	// bind the texture and PBO
	glBindTexture(GL_TEXTURE_2D, texturesID[_currentBuffer]); //solo se necesita una textura
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pboID[_currentBuffer]);

	// copy pixels from PBO to texture object
	// Use offset instead of pointer.
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, WIDTH, HEIGHT, GL_RGBA, GL_UNSIGNED_BYTE, 0);


	// bind PBO to update texture source
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pboID[_nextBuffer]);

	// Note that glMapBufferARB() causes sync issue.
	// If GPU is working with this buffer, glMapBufferARB() will wait(stall)
	// until GPU to finish its job. To avoid waiting (idle), you can call
	// first glBufferDataARB() with NULL pointer before glMapBufferARB().
	// If you do that, the previous data in PBO will be discarded and
	// glMapBufferARB() returns a new allocated pointer immediately
	// even if GPU is still working with the previous data.
	glBufferData(GL_PIXEL_UNPACK_BUFFER, WIDTH * HEIGHT * RGBA, 0, GL_STREAM_DRAW);

	// map the buffer object into client's memory

	_screenBuffer = (GLubyte*)glMapBuffer(GL_PIXEL_UNPACK_BUFFER, GL_READ_WRITE);
	if (_screenBuffer)
	{
		// update data directly on the mapped buffer
		//updatePixels(ptr);
		draw();
		glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER); // release the mapped buffer
	}

	// it is good idea to release PBOs with ID 0 after use.
	// Once bound with 0, all pixel operations are back to normal ways.
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
}

//Called whenever the window is resized. The new window size is given, in pixels.
//This is an opportunity to call glViewport or glScissor to keep up with the change in size.
void BaseApplication::reshape(int w, int h)
{
	//glViewport(0, 0, (GLsizei)w, (GLsizei)h);
	//mProjectionMatrix = glm::perspective(45.0f, (float)w / (float)h, 1.0f, 10000.f);
}



									 
void BaseApplication::cursor_position(double xpos, double ypos)
{
	int width, heigth;

	glfwGetWindowSize(window, &width, &heigth);
}


void BaseApplication::fakeBuffers()
{
	for (int i = 0; i < WIDTH*HEIGHT*RGBA; i+=4)
	{
		Buffers[1][i] = 255;
		Buffers[1][i + 1] = 0;
		Buffers[1][i + 2] = 0;
		Buffers[1][i + 3] = 255;

		Buffers[0][i] = 0;
		Buffers[0][i + 1] = 255;
		Buffers[0][i + 2] = 0;
		Buffers[0][i + 3] = 255;
	}
}

void BaseApplication::swapbuffers()
{
	_currentBuffer = 1 - _currentBuffer;
	_nextBuffer = 1 - _currentBuffer;
}


void BaseApplication::putPixel(GLubyte *buffer, const int& x, const int& y, const char& R, const char& G, const char& B, const char& A)
{
	int offset = (x + (y * WIDTH)) * RGBA;
	if (offset < 0 || offset > WIDTH * HEIGHT *RGBA)
		return;

	buffer[offset]	 = R;
	buffer[offset + 1] = G;
	buffer[offset + 2] = B;
	buffer[offset + 3] = A;

}

void BaseApplication::putPixel(const int& x, const int& y, const char& R, const char& G, const char& B, const char& A)
{
	putPixel(_screenBuffer, x, y, R, G, B, A);
}

void BaseApplication::putPixel(const int& x, const int& y, Color c)
{
	putPixel(x, y, c.R(),c.G(), c.B(), c.A());
}


///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

//void BaseApplication::Line(int x0, int y0, int x1, int y1)
//{
//	int dy = y1 - y0;
//	int dx = x1 - x0;
//	//if (dx > 0 && dy < 0 && abs(dx) >= abs(dy))
//	{
//		int E = 2 * dy;
//		int NE = 2 * (dy - dx);
//		int d = 2 * dy - dx;
//		putPixel(x0, y0, 0, 255, 255, 255);
//		int x = x0;
//		int y = y0;
//		while (x <= x1)
//		{
//			if (d > 0)
//			{
//				++y;
//				d += NE;
//			}
//			else
//			{
//				d += E;
//			}
//			++x;
//			putPixel(256 + x, 256 - y, 0, 255, 255, 255);
//		}
//	}
//}
//
//// Dibuja lineas de 45 a 90 grados en el sentido del reloj. 01:30 a 00:00 en el reloj
//void BaseApplication::Line2(int x0, int y0, int x1, int y1)
//{
//	int dy = y1 - y0;
//	int dx = x1 - x0;
//	//if (dx > 0 && dy < 0 && abs(dx) < abs(dy))
//	{
//		int E = 2 * dy;
//		int NE = 2 * (dy - dx);
//		int d = 2 * dy - dx;
//		putPixel(x0, y0, 0, 255, 255, 255);
//		int x = x0;
//		int y = y0;
//		while (x <= x1)
//		{
//			if (d > 0)
//			{
//				++y;
//				d += NE;
//			}
//			else
//			{
//				d += E;
//			}
//			putPixel(256 + y, 256 - x, 0, 255, 255, 255);
//			++x;
//		}
//	}
//}
//
//// Dibuja lineas de 90 a 135 grados en el sentido del reloj. 00:00 a 10:30 en el reloj
//void BaseApplication::Line3(int x0, int y0, int x1, int y1)
//{
//	int dy = y1 - y0;
//	int dx = x1 - x0;
//	int E = 2 * dy;
//	int NE = 2 * (dy - dx);
//	int d = 2 * dy - dx;
//	putPixel(x0, y0, 0, 255, 255, 255);
//	int x = x0;
//	int y = y0;
//	while (x <= x1)
//	{
//		if (d > 0)
//		{
//			++y;
//			d += NE;
//		}
//		else
//		{
//			d += E;
//		}
//		putPixel(256 - y, 256 - x, 0, 255, 255, 255);
//		++x;
//	}
//}
//
//
//// Dibuja lineas de 135 a 180 grados en el sentido del reloj. 10:30 a 09:00 en el reloj
//void BaseApplication::Line4(int x0, int y0, int x1, int y1)
//{
//	int dy = y1 - y0;
//	int dx = x1 - x0;
//	int E = 2 * dy;
//	int NE = 2 * (dy - dx);
//	int d = 2 * dy - dx;
//	putPixel(x0, y0, 0, 255, 255, 255);
//	int x = x0;
//	int y = y0;
//	while (x <= x1)
//	{
//		if (d > 0)
//		{
//			++y;
//			d += NE;
//		}
//		else
//		{
//			d += E;
//		}
//		putPixel(256 - x, 256 - y, 0, 255, 255, 255);
//		++x;
//	}
//}
//
//
//// Dibuja lineas de 135 a 180 grados en el sentido del reloj. 09:00 a 07:30 en el reloj
//void BaseApplication::Line5(int x0, int y0, int x1, int y1)
//{
//	int dy = y1 - y0;
//	int dx = x1 - x0;
//	int E = 2 * dy;
//	int NE = 2 * (dy - dx);
//	int d = 2 * dy - dx;
//	putPixel(x0, y0, 0, 255, 255, 255);
//	int x = x0;
//	int y = y0;
//	while (x <= x1)
//	{
//		if (d > 0)
//		{
//			++y;
//			d += NE;
//		}
//		else
//		{
//			d += E;
//		}
//		putPixel(256 - x, 256 + y, 0, 255, 255, 255);
//		++x;
//	}
//}
//
//// Dibuja lineas de 180 a 225 grados en el sentido del reloj. 07:30 a 06:00 en el reloj
//void BaseApplication::Line6(int x0, int y0, int x1, int y1)
//{
//	if (x0 < y1)
//	{
//		int dy = y1 - y0;
//		int dx = x1 - x0;
//		int E = 2 * dy;
//		int NE = 2 * (dy - dx);
//		int d = 2 * dy - dx;
//		putPixel(x0, y0, 0, 255, 255, 255);
//		int x = x0;
//		int y = y0;
//		while (x <= x1)
//		{
//			if (d > 0)
//			{
//				++y;
//				d += NE;
//			}
//			else
//			{
//				d += E;
//			}
//			putPixel(256 - y, 256 + x, 0, 255, 255, 255);
//			++x;
//		}
//	}
//}
//
//// Dibuja lineas de 225 a 270 grados en el sentido del reloj. 06:00 a 04:30 en el reloj
//void BaseApplication::Line7(int x0, int y0, int x1, int y1)
//{
//	int dy = y1 - y0;
//	int dx = x1 - x0;
//	int E = 2 * dy;
//	int NE = 2 * (dy - dx);
//	int d = 2 * dy - dx;
//	putPixel(x0, y0, 0, 255, 255, 255);
//	int x = x0;
//	int y = y0;
//	while (x <= x1)
//	{
//		if (d > 0)
//		{
//			++y;
//			d += NE;
//		}
//		else
//		{
//			d += E;
//		}
//		putPixel(256 + y, 256 + x, 0, 255, 255, 255);
//		++x;
//	}
//}
//
//// Dibuja lineas de 270 a 315 grados en el sentido del reloj. 04:30 a 03:00 en el reloj
//void BaseApplication::Line8(int x0, int y0, int x1, int y1)
//{
//	//x0 = WIDTH / 2 + x0;
//	//y0 = HEIGHT / 2 + y0;
//	//x1 = WIDTH / 2 + x1;
//	//y1 = HEIGHT / 2 + y1;
//	int dy = y1 - y0;
//	int dx = x1 - x0;
//	int E = 2 * dy;
//	int NE = 2 * (dy - dx);
//	int d = 2 * dy - dx;
//	putPixel(x0, y0, 0, 255, 255, 255);
//	int x = x0;
//	int y = y0;
//	while (x <= x1)
//	{
//		if (d > 0)
//		{
//			++y;
//			d += NE;
//		}
//		else
//		{
//			d += E;
//		}
//		++x;
//		putPixel(256 + x, 256 + y, 0, 255, 255, 255);
//	}
//}
//
//
//void BaseApplication::TrescientosSesentaLineas()
//{
//	int x;
//	int y;
//	float PI = 3.1416;
//	for (int angulo = 0; angulo < 360; ++angulo)
//	{
//		x = 256 * cos(float(angulo) * PI / 180.0);
//		y = 256 * sin(float(angulo) * PI / 180.0);
//		// Primer Octante
//		Line(0, 0, x, y);
//		// Segundo Octante
//		Line2(0, 0, x, y);
//		// Tercer Octante
//		Line3(0, 0, x, y);
//		// Cuarto Octante
//		Line4(0, 0, x, y);
//		// Quinto Octante
//		Line5(0, 0, x, y);
//		// Sexto Octante
//		Line6(0, 0, x, y);
//		// Septimo Octante
//		Line7(0, 0, x, y);
//		// Octavo Octante
//		Line8(0, 0, x, y);
//	}
//}
void BaseApplication::moveTo(int x, int y)
{
	atVec2.x = x;
	atVec2.y = y;
}

void BaseApplication::lineTo(int x, int y)
{
	float _x, _y, avance;

	float dx = abs(x - atVec2.x);
	float dy = abs(y - atVec2.y);
	if (dx >= dy)
	{
		avance = dx;
	}
	else
	{
		avance = dy;
	}
	// Incremento en x
	dx = (x - atVec2.x) / avance;
	// Incremento en y
	dy = (y - atVec2.y) / avance;
	_x = atVec2.x + 0.5; /* El 0.5 es agregado para redondear los valores si sus fracciones son 0.5 o mayores */
	_y = atVec2.y + 0.5; /* El 0.5 es agregado para redondear los valores si sus fracciones son 0.5 o mayores */
	for (int i = 1; i <= avance; i++)
	{
		putPixel(_x, _y,0,255,255,255);
		_x += dx;
		_y += dy;
	}
	moveTo(x, y);
}

void BaseApplication::Poligonos()
{
	moveTo(vertices[0].x, vertices[0].y);
	for (int i = 0; i < vertices.size() - 1; i++)
	{
		lineTo(vertices[i].x, vertices[i].y);
	}
	lineTo(vertices[0].x, vertices[0].y);
}

//void Application::setRunStart() { _runStart = true; }







void BaseApplication::clearScreen()
{
	for (int i = 0; i < WIDTH * HEIGHT * RGBA; i += 4)
	{
		Buffers[1][i] = 0;
		Buffers[1][i + 1] = 0;
		Buffers[1][i + 2] = 0;
		Buffers[1][i + 3] = 0;

		Buffers[0][i] = 0;
		Buffers[0][i + 1] = 0;
		Buffers[0][i + 2] = 0;
		Buffers[0][i + 3] = 0;
	}
}






void BaseApplication::setup()
{
	std::cout << "Introduce el numero de lados: ";
	std::cin >> _lados;
	int angulo = 0;
	int incremento = 360 / _lados;
	int radio = 75;
	double PI = 3.14159265358979323846;
	Vector2 primitivesVector;
	moveTo(256, 256);
	primitivesVector.x = atVec2.x;
	primitivesVector.y = atVec2.y;
	for (int i = 0; i <= _lados; i++)
	{
		primitivesVector.x += radio * cos(angulo * PI / 180);
		primitivesVector.y += radio * sin(angulo * PI / 180);
		angulo += incremento;
		vertices.push_back(primitivesVector);
	}
}

void BaseApplication::update()
{
}

void BaseApplication::draw()
{
	//for (int j = 0; j < WIDTH; ++j)
	//{
	//	putPixel(j, WIDTH/2, 0, 0, 255, 255);
	//}

	//for (int j = 0; j < HEIGHT; ++j)
	//{
	//	putPixel( WIDTH/2, j, 0, 0, 255, 255);
	//}

	////for (int j = 0, int y = 0; j < WIDTH, y < HEIGHT; ++j, y++)
	////{
	////	putPixel(j, y, 255, 0, 0, 255);
	////}
	////putPixel(50, 10, 0, 0, 255, 255);
	//int j = 0;
	//for (size_t i = 0; i < WIDTH; i++)
	//{
	//	putPixel(i,j, 255, 0, 0, 255);
	//	j++;
	//}

	//int k = 0;
	//for (size_t i = WIDTH; i > 0; i--)
	//{
	//	putPixel(i, k, 255, 0, 0, 255);
	//	k++;
	//}
	//TrescientosSesentaLineas();
	Poligonos();
}


void BaseApplication::keyboard(int key, int scancode, int action, int mods)
{
	const float inc = 1.0;
	switch (action)
	{		
	case GLFW_PRESS:
		switch (key)
		{
		
		case GLFW_KEY_ESCAPE: glfwSetWindowShouldClose(window, GL_TRUE); break;		
		case GLFW_KEY_T: _currentBuffer = 1 - _currentBuffer; break;
		case GLFW_KEY_P:
			_drawMode = ++_drawMode % 3;
			switch (_drawMode)
			{
				case 0: glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); break;
				case 1: glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); break;
				case 2:	glPolygonMode(GL_FRONT, GL_FILL);
						glPolygonMode(GL_BACK, GL_LINE);
						break;
			}
		}
		//break;
		
	} //switch (action)
}

