#pragma once

class Vector3
{
public:
	float x;
	float y;
	float z;

	Vector3() : x(0), y(0), z(0)
	{
	}

	Vector3(float x, float y, float z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

	Vector3 operator +(Vector3 b) const
	{
		return Vector3(x + b.x, y + b.y, z + b.z);
	}

	Vector3 operator -(Vector3 b) const
	{
		return Vector3(x - b.x, y - b.y, z - b.z);
	}

	Vector3 operator /(float d) const
	{
		return Vector3(x / d, y / d, z / d);
	}

	Vector3& operator +=(Vector3 b);

	Vector3& operator -=(Vector3 b);

	Vector3& operator /=(float d);

	Vector3& operator *=(class Matrix rhs);

public:
	static Vector3 zeroVector;
	static Vector3 oneVector;
	static Vector3 upVector;
	static Vector3 downVector;
	static Vector3 leftVector;
	static Vector3 rightVector;
	static Vector3 forwardVector;
	static Vector3 backVector;
};
