#pragma once
#include "Vector3.h"

class Matrix
{
public:
	Vector3 row1, row2, row3;
	static Matrix multiply_matrix(Matrix x_matrix, Matrix y_matrix);;

	Matrix() : row1(1, 0, 0), row2(0, 1, 0), row3(0, 0, 1)
	{
	}

	Vector3& operator*(Vector3 vector);
	void Transformation2D(float x, float y);
	void Scale2D(float x, float y);
	void Rotation2D(float angle);
};
