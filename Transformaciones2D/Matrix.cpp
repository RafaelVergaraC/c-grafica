#include "Matrix.h"
#include <cmath>

const float PI = 3.14159265358979323846f;
const float RadToDeg = PI / 180;

Matrix Matrix::multiply_matrix(Matrix m1_matrix, Matrix m2_matrix)
{
	Matrix r_matrix;
	r_matrix.row1.x = ((m1_matrix.row1.x * m2_matrix.row1.x) +
		(m1_matrix.row1.y * m2_matrix.row2.x) + (m1_matrix.row1.z * m2_matrix.row3.x));
	r_matrix.row1.y = ((m1_matrix.row1.x * m2_matrix.row1.y) +
		(m1_matrix.row1.y * m2_matrix.row2.y) + (m1_matrix.row1.z * m2_matrix.row3.y));
	r_matrix.row1.z = ((m1_matrix.row1.x * m2_matrix.row1.z) +
		(m1_matrix.row1.y * m2_matrix.row2.z) + (m1_matrix.row1.z * m2_matrix.row3.z));


	r_matrix.row2.x = ((m1_matrix.row2.x * m2_matrix.row1.x) +
		(m1_matrix.row2.y * m2_matrix.row2.x) + (m1_matrix.row2.z * m2_matrix.row3.x));
	r_matrix.row2.y = ((m1_matrix.row2.x * m2_matrix.row1.y) +
		(m1_matrix.row2.y * m2_matrix.row2.y) + (m1_matrix.row2.z * m2_matrix.row3.y));
	r_matrix.row2.z = ((m1_matrix.row2.x * m2_matrix.row1.z) +
		(m1_matrix.row2.y * m2_matrix.row2.z) + (m1_matrix.row2.z * m2_matrix.row3.z));


	r_matrix.row3.x = ((m1_matrix.row3.x * m2_matrix.row1.x) +
		(m1_matrix.row3.y * m2_matrix.row2.x) + (m1_matrix.row3.z * m2_matrix.row3.x));
	r_matrix.row3.y = ((m1_matrix.row3.x * m2_matrix.row1.y) +
		(m1_matrix.row3.y * m2_matrix.row2.y) + (m1_matrix.row3.z * m2_matrix.row3.y));
	r_matrix.row3.z = ((m1_matrix.row3.x * m2_matrix.row1.z) +
		(m1_matrix.row3.y * m2_matrix.row2.z) + (m1_matrix.row3.z * m2_matrix.row3.z));
	return r_matrix;
}

Vector3& Matrix::operator*(Vector3 vector)
{
	Vector3 vector3;
	vector3.x = (float)((double)row1.x * (double)vector.x + (double)row1.y * (double)vector.y + (double)row1.z * (double)
		vector.z);
	vector3.y = (float)((double)row2.x * (double)vector.x + (double)row2.y * (double)vector.y + (double)row2.z * (double)
		vector.z);
	vector3.z = (float)((double)row3.x * (double)vector.x + (double)row3.y * (double)vector.y + (double)row3.z * (double)
		vector.z);
	return vector3;
}

void Matrix::Transformation2D(float x, float y)
{
	row1.z = x;
	row2.z = y;
}

void Matrix::Scale2D(float x, float y)
{
	row1.x = x;
	row2.y = y;
}

void Matrix::Rotation2D(float angle)
{
	row1.x = (cos(angle * RadToDeg));
	row2.x = (sin(angle * RadToDeg));
	row1.y = 0 - row2.x;
	row2.y = row1.x;
}
