#include "Vector3.h"
#include "Matrix.h"

Vector3 Vector3::zeroVector(0.0f, 0.0f, 0.0f);
Vector3 Vector3::oneVector(1, 1, 1);
Vector3 Vector3::upVector(0.0f, 1, 0.0f);
Vector3 Vector3::downVector(0.0f, -1, 0.0f);
Vector3 Vector3::leftVector(-1, 0.0f, 0.0f);
Vector3 Vector3::rightVector(1, 0.0f, 0.0f);
Vector3 Vector3::forwardVector(0.0f, 0.0f, 1);
Vector3 Vector3::backVector(0.0f, 0.0f, -1);

Vector3& Vector3::operator+=(Vector3 b)
{
	Vector3 a(x + b.x, y + b.y, z + b.z);
	this->x = a.x;
	this->y = a.y;
	this->z = a.z;
	return *this;
}

Vector3& Vector3::operator-=(Vector3 b)
{
	Vector3 a(x - b.x, y - b.y, z - b.z);
	this->x = a.x;
	this->y = a.y;
	this->z = a.z;
	return *this;
}

Vector3& Vector3::operator/=(float d)
{
	Vector3 a(x / d, y / d, z / d);
	this->x = a.x;
	this->y = a.y;
	this->z = a.z;
	return *this;
}

Vector3& Vector3::operator*=(Matrix rhs)
{
	Vector3 a = rhs * (*this);
	this->x = a.x;
	this->y = a.y;
	this->z = a.z;
	return *this;
}
